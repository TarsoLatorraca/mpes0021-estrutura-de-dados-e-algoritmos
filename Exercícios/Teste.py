import os                           #Necessário para limpar a tela
import random                       #Necessário para utilizar a função random
import time                         #Necessário para utilizar a função sleep

#Variáveis que armazenam as opções dos jogadores em relação ao jogo.
opjogo = 0
jogada = 0
totalj = 0
totalc = 0

#Variáveis da jogada atual e do total de pontos.
total1 = 0
total2 = 0

print("Bem Vindo ao BlackJack em Python!! \n")
#Vez do usuário.
opjogo='S';
os.system('clear')
print("Você tem que fazer 21 pontos para ganhar, porém sem estourar esse limite, podendo parar após cada jogada.\n");
print
totalj=0
while opjogo=='s' or opjogo=='S':   #Laço que deixa as jogadas a critério do usuário.
    opjogo = input("Deseja fazer a jogada?[S/N] \n")
    if opjogo=='n' or opjogo=='N':
        break
    jogada= random.randint(1,10) #Gera números aleatórios de 1 até 10.
    totalj=(totalj+jogada)
    if totalj >= 21:
        break
    print
    print("Você tirou {0} e até agora marcou {1} pontos.\n".format(jogada,totalj))
    print

print("Você marcou {0} pontos, vamos ver o computador...\n".format(totalj))
#Vez do Computador
print("Agora é minha vez de jogar. Vejo que você fez {0} pontos...\n".format(totalj))
totalc=0
while (totalc < 21):
    jogada= random.randint(1,10) #Gera números aleatórios de 1 até 10.
    totalc=(totalc+jogada)
    if totalj > 21:
            break
    print("Tirei {0} pontos e pretendo continuar jogando, pois ainda estou com {1}.\n".format(jogada,totalc))
    time.sleep(1.2)

print

print("O jogador terminou com {0} pontos e o computador com {1} pontos, portanto...".format(totalj,totalc))
if totalj == 21:
    print("O jogador ganhou, fazendo os gloriosos 21 pontos =D...\n")
elif totalc == 21:
    print("O computador ganhou, fazendo os gloriosos 21 pontos =D...\n")
elif totalc > 21 and totalj <= 21:
    print("O computador tem um número maior de pontos do que é permitido...O jogador vence.\n")
elif totalj > 21 and totalc <= 21:
    print("O jogador tem um número maior de pontos do que é permitido...O computador vence.\n")
elif 21 - totalc > 21 - totalj:
    print("O computador vence por estar mais perto de 21.\n")
elif 21 - totalc < 21 - totalj:
    print("O jogador vence por estar mais perto de 21.\n")
    print