import time

def ordenar(lista):
    inicio = time.time()
    for i in range(1, len(lista)):
        menor = lista[i]
        j = i - 1
        while (j >= 0) and (lista[j] > menor):
            lista[j + 1] = lista[j]
            j = j - 1

        lista[j + 1] = menor

    # print(lista)
    fim = time.time()

    print("Tempo de execução do InsertionSort: {0}".format(fim - inicio))