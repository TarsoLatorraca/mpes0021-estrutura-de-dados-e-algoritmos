# encoding: utf-8

import random
import time
import sys

posicoes = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"]
naipes = ["♠", "♥", "♢", "♧"]
cartas = []
pontos_jogador = 0
pontos_banca = 0

for naipe in naipes:
    for posicao in posicoes:
        cartas.append(posicao + naipe)

def pontuacao(posicao):
    carta = posicao[0:-1]
    pontos_carta = 0

    if carta in posicoes[0]:
        if pontos_jogador > 10:
            pontos_carta = 1
        if pontos_banca > 10:
            pontos_carta = 1
        else:
            pontos_carta = 11
    elif carta in posicoes[1:9]:
        pontos_carta = int(carta)
    elif carta in posicoes[9:]:
        pontos_carta = 10

    return pontos_carta

def fim(pontosJogador):
    if pontosJogador == 21:
        print("BlackJack!!! Você venceu com 21 pontos... Parabéns!")
    elif pontosJogador > 21:
        print("Você estourou os 21 pontos... A Banca vence!!!")
    else:
        print('Não sei.')
    print(
        "\n"
        "🂠 🂡 🂢 🂣 🂤 🂥 🂦 🂧 🂨 🂩 🂪 🂫 🂬 🂭 🂠 🂡 🂢 🂣 🂤 🂥 🂦 🂧 🂨 🂩 🂪 🂫 🂬 🂭 🂠 🂡 🂢 🂣 🂤 🂥 🂦 🂧 🂨 🂩 🂪 🂫 🂬 🂭 🂠 🂡 🂢 🂣 🂤 🂥 🂦 🂧 🂨 🂩 🂪 🂫 🂬 🂭 🂠"
    )
    sys.exit()

print(
    "==========================================================================================\n"
    "Aluno: Tarso Latorraca Casadei"
    "\n"
    "Matrícula 20161023866"
    "\n"
    "\n"
    "BlackJack.py: Trabalho da disciplina MPES0021 - ESTRUTURA DE DADOS E ALGORITMOS"
    "\n"
    "\n"
    "Para informações sobre regras do jogo, acesse http://pt.blackjack.org/regras-do-blackjack/"
    "\n"
    "==========================================================================================\n"
    )

random.shuffle(cartas)

carta1 = cartas.pop()
carta2 = cartas.pop()
carta3 = cartas.pop()
carta4 = cartas.pop()

carta_atual = 0

pontos_jogador = pontuacao(carta1) + pontuacao(carta2)
pontos_banca = pontuacao(carta3) + pontuacao(carta4)

opcao_jogador = 'S'
print(
    "🂠 🂡 🂢 🂣 🂤 🂥 🂦 🂧 🂨 🂩 🂪 🂫 🂬 🂭 🂠 🂡 🂢 🂣 🂤 🂥 🂦 🂧 🂨 🂩 🂪 🂫 🂬 🂭 🂠 🂡 🂢 🂣 🂤 🂥 🂦 🂧 🂨 🂩 🂪 🂫 🂬 🂭 🂠 🂡 🂢 🂣 🂤 🂥 🂦 🂧 🂨 🂩 🂪 🂫 🂬 🂭 🂠"
    "\n"
    "\n"
    "Início do jogo."
    "\n"
    "🂻 Suas 2 cartas iniciais são: {0} e {1}"
    "\n"
    "Sua pontuação inicial é: {2}"
    "\n"
        .format(carta1,carta2,pontos_jogador)
)

while opcao_jogador == 's' or opcao_jogador == 'S' and pontos_jogador < 21:
    opcao_jogador = input("Deseja comprar outra carta?\n[S/N]: ")

    if opcao_jogador == 'n' or opcao_jogador == 'N':
        break

    carta_atual = cartas.pop()
    pontos_jogador = pontos_jogador + pontuacao(carta_atual)

    print(
        "\n"
        "🂻 {0}        Sua pontuação atual é {1} pontos."
        "\n"
            .format(carta_atual, pontos_jogador))

    if pontos_jogador >= 21:
        fim(pontos_jogador)

    time.sleep(1.2)

print(
    "\n"
    "Você escolheu parar com {0} pontos. Agora é a vez da Banca jogar!"
        .format(pontos_jogador)
)

time.sleep(1.2)

print(
    "\n"
    "🂠 🂡 🂢 🂣 🂤 🂥 🂦 🂧 🂨 🂩 🂪 🂫 🂬 🂭 🂠 🂡 🂢 🂣 🂤 🂥 🂦 🂧 🂨 🂩 🂪 🂫 🂬 🂭 🂠 🂡 🂢 🂣 🂤 🂥 🂦 🂧 🂨 🂩 🂪 🂫 🂬 🂭 🂠 🂡 🂢 🂣 🂤 🂥 🂦 🂧 🂨 🂩 🂪 🂫 🂬 🂭 🂠"
    "\n"
    "\n"
    "🂻 Cartas da Banca: {0} e {1}"
    "\n"
    "Pontuação inicial da Banca: {2}"
    "\n"
        .format(carta3,carta4,pontos_banca)
)

time.sleep(1.2)

while (pontos_banca < pontos_jogador):
    carta_atual = cartas.pop()
    pontos_banca = pontos_banca + pontuacao(carta_atual)

    print(
        "\n"
        "🂻 {0}        A pontuação atual da Banca é {1} pontos."
            .format(carta_atual, pontos_banca))

    if pontos_banca >=21:
        break

    time.sleep(1.2)

print(
    "\n"
    "🂠 🂡 🂢 🂣 🂤 🂥 🂦 🂧 🂨 🂩 🂪 🂫 🂬 🂭 🂠 🂡 🂢 🂣 🂤 🂥 🂦 🂧 🂨 🂩 🂪 🂫 🂬 🂭 🂠 🂡 🂢 🂣 🂤 🂥 🂦 🂧 🂨 🂩 🂪 🂫 🂬 🂭 🂠 🂡 🂢 🂣 🂤 🂥 🂦 🂧 🂨 🂩 🂪 🂫 🂬 🂭 🂠"
    "Você marcou {0} pontos e a Banca fez {1} pontos, portanto..."
    "\n"
        .format(pontos_jogador, pontos_banca)
)

time.sleep(1.8)

if pontos_banca == 21:
    print("BlackJack da Banca!!! Você perdeu.")
elif pontos_banca == pontos_jogador:
    print("Empate!!!")
elif pontos_banca > 21:
    print("A Banca estourou os 21 pontos... Parabéns, você venceu!!!")
else:
    print("A Banca marcou mais pontos. Você perdeu!!!")