# encoding: utf-8

import Jogo

def jogador(pontos_jogador):
    if Jogo.pontos_jogador == 21:
        print("BlackJack!!! Você venceu com 21 pontos... Parabéns!")
    else:
        print("Você estourou os 21 pontos... A Banca vence!!!")
    print(
        "==========================================================================================\n"
    )

def banca(pontos_banca):
    if Jogo.pontos_banca == 21:
        print("BlackJack da Banca!!! Você perdeu.")
    elif Jogo.pontos_banca == Jogo.pontos_jogador:
        print("Empate!!!")
    elif Jogo.pontos_banca > 21:
        print("A Banca estourou os 21 pontos... Parabéns, você venceu!!!")
    else:
        print("A Banca marcou mais pontos. Você perdeu!!!")
    print(
        "==========================================================================================\n"
    )