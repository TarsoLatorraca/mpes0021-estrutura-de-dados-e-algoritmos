# encoding: utf-8

import Teste

posicoes = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"]
naipes = ["♠", "♥", "♢", "♧"]
cartas = []

def montar():
    for naipe in naipes:
        for posicao in posicoes:
            cartas.append(posicao + naipe)
    return cartas

# def embaralhar(self):
#     qtdCartas = len(self.cartas)
#     for i in range(qtdCartas):
#         j = random.randrange(i, qtdCartas)
#         self.cartas[i], self.cartas[j] = self.cartas[j], self.cartas[i]

def pontuacao(posicao):
    pontos_carta = 0
    carta = posicao[0:-1]

    if carta in posicoes[0]:
        if Teste.pontos_jogador or Teste.pontos_banca > 10:
            pontos_carta = 1
        else:
            pontos_carta = 11
    elif carta in posicoes[1:9]:
        pontos_carta = int(carta)
        return pontos_carta
    elif carta in posicoes[9:]:
        pontos_carta = 10
        return pontos_carta
    else:
        pontos_carta = 1
        return pontos_carta